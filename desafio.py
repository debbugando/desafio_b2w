# Desafio Técnico - Dev. Back-End
# Instruções gerais
# Olá, tudo bem! Dando continuidade ao nosso processo, temos um desafio para te propor! \o/ Nossos associados são aficionados por Star Wars e com isso, queremos criar um jogo com algumas informações da franquia. Para possibilitar a equipe de front criar essa aplicação, queremos desenvolver uma API que contenha os dados dos planetas. Requisitos: - A API deve ser REST - Para cada planeta, os seguintes dados devem ser obtidos do banco de dados da aplicação, sendo inserido manualmente: Nome Clima Terreno - Para cada planeta também devemos ter a quantidade de aparições em filmes, que podem ser obtidas pela API pública do Star Wars: https://swapi.co/ Funcionalidades desejadas: - Adicionar um planeta (com nome, clima e terreno) - Listar planetas - Buscar por nome - Buscar por ID - Remover planeta Linguagens que usamos: Java ou Go Bancos que usamos: MongoDB, Cassandra, DynamoDB, Datomic E lembre-se! Um bom software é um software bem testado. May the force be with you!
# Desafio back
#
# Olá!
# Dando continuidade ao nosso processo, temos um desafio para te propor! \o/
# Nossos associados são aficionados por Star Wars e com isso, queremos criar um jogo com algumas informações da franquia.
# Para possibilitar a equipe de front criar essa aplicação, queremos desenvolver uma API que contenha os dados dos planetas.
#
# Requisitos:
# - A API deve ser REST
# - Para cada planeta, os seguintes dados devem ser obtidos do banco de dados da aplicação, sendo inserido manualmente:
# Nome, Clima, Terreno
# - Para cada planeta também devemos ter a quantidade de aparições em filmes, que podem ser obtidas pela API pública do Star Wars: https://swapi.co/
#
# Funcionalidades desejadas:
# Adicionar um planeta (com nome, clima e terreno)
# - Listar planetas
# - Buscar por nome
# - Buscar por ID
# - Remover planetaLinguagens que usamos: Java, Go, Clojure, Node, Python
# Bancos que usamos: MongoDB, Cassandra, DynamoDB, Datomic. E lembre-se! Um bom software é um software bem testado.
# May the force be with you!

import pymongo
import json
from bson import ObjectId
from datetime import datetime
from flask import Flask, request, jsonify, abort

START = 1
LIMIT = 10

# Conexão do banco de dados
client = pymongo.MongoClient('mongodb://localhost:27017/')
db = client['desafio']
# Cria a collection de planetas
collection = db["planetas"]
# Limpa o banco
collection.delete_many({})

'''
# Abordagem usando a api do SWAPI
# Busca os planetas pela api do SWAPI
get_planets = requests.get("https://swapi.co/api/planets").json()
# Quantidade de planetas para o loop
num_pages = get_planets['count']

# Loop dos planetas
for page in range(1, num_pages + 1):
    planet_data = requests.get("https://swapi.co/api/planets/" + str(page)).json()
    collection.insert_one(planet_data)
'''

# Abordagem com arquivo em json e seus planetas
with open('./planetas.json') as json_file:
    data = json.load(json_file)
collection.insert_many(data)

app = Flask(__name__)

# Função para paginar os resultados
def get_paginated_list(results, url, start, limit):
    start = int(start)
    limit = int(limit)
    count = len(results)
    if count < start or limit < 0:
        abort(404)
    # Cria o response
    obj = {'start': start, 'limit': limit, 'count': count}

    # Configura a página anterior
    if start == 1:
        obj['previous'] = ''
    else:
        start_copy = max(1, start - limit)
        limit_copy = start - 1
        obj['previous'] = url + '?start=%d&limit=%d' % (start_copy, limit_copy)

    # Configura a próxima página
    if start + limit > count:
        obj['next'] = ''
    else:
        start_copy = start + limit
        obj['next'] = url + '?start=%d&limit=%d' % (start_copy, limit)

    # Retorno do resultado
    obj['results'] = results[(start - 1):(start - 1 + limit)]
    return obj


@app.route('/', methods=['GET'])
@app.route('/planets', methods=['GET'])
@app.route('/planets/', methods=['GET'])
@app.route('/planets/all', methods=['GET'])
def list():
    # Busca os dados de todos os planetas por paginação
    planetas = collection.find({}, {'_id': 0})
    planetas.sort('name', 1)

    json_obj = []
    for planeta in planetas:
        obj = {
            'nome': planeta['name'],
            'terreno': planeta['terrain'],
            'filmes': planeta['films'] if len(planeta['films']) > 0 else '',
            'qtd_filmes': len(planeta['films']),
            'clima': planeta['climate']
        }
        json_obj.append(obj)

    # Caso entre na rota de todos
    if request.url_rule.rule == '/planets/all':
        return jsonify(results=json_obj), 200
    else:
        return jsonify(get_paginated_list(
            json_obj,
            '/planets',
            start=request.args.get('start', START),
            limit=request.args.get('limit', LIMIT)
        )), 200

# Rota para criar planeta
@app.route('/planets/create', methods=['POST'])
def create():
    try:
        planet = collection.insert_one({
            'name': request.form['name'].capitalize(),
            'terrain': request.form['terrain'],
            'climate': request.form['climate'],
            'created': datetime.now(),
            'edited': datetime.now(),
        })
        return jsonify({'message': 'success'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 404

# Rota para exibir planeta por id
@app.route('/planets/<id>', methods=['GET'])
def show(id):
    try:
        json_obj = []
        if (ObjectId.is_valid(id)):
            planeta = collection.find_one({'_id': ObjectId(id)})
            if (planeta):
                obj = {
                    'nome': planeta['name'],
                    'terreno': planeta['terrain'],
                    'filmes': planeta['films'] if len(planeta['films']) > 0 else '',
                    'qtd_filmes': len(planeta['films']),
                    'clima': planeta['climate']
                }
                json_obj.append(obj)
        return jsonify(json_obj), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 404

# Rota para exibir planeta por nome
@app.route('/planets/search/<filtro>', methods=['GET'])
def search(filtro):
    try:
        planetas = collection.find({'name': {'$regex': filtro, '$options': 'i'}})
        planetas.sort('name', 1)

        json_obj = []
        for planeta in planetas:
            obj = {
                'nome': planeta['name'],
                'terreno': planeta['terrain'],
                'filmes': planeta['films'] if len(planeta['films']) > 0 else '',
                'qtd_filmes': len(planeta['films']),
                'clima': planeta['climate']
            }
            json_obj.append(obj)

        return jsonify(get_paginated_list(
                json_obj,
                '/planets',
                start=request.args.get('start', START),
                limit=request.args.get('limit', LIMIT)
            )), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 404

# Rota para remover planeta
@app.route('/planets/delete/<id>', methods = ['DELETE', 'POST'])
def remove(id):
    try:
        if (ObjectId.is_valid(id)):
            dados = collection.delete_one({
                '_id': ObjectId(id)
            })
            return jsonify({
                'message': 'success',
                'register_deleted': dados.deleted_count,
                'id_deleted': id
            }), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 404

@app.route('/planets/edit/<id>', methods = ['PUT', 'POST', 'PATCH'])
def update(id):
    try:
        if (ObjectId.is_valid(id)):
            dados = collection.update_one(
                {'_id': ObjectId(id)},
                {
                    '$set': {
                        'name': request.form['nome'],
                        'terrain': request.form['terreno'],
                        'climate': request.form['clima'],
                        'edited': datetime.now(),
                    }
                })
            planeta = collection.find_one({'_id': ObjectId(id)})
            obj = {
                'nome': planeta['name'],
                'terreno': planeta['terrain'],
                'filmes': planeta['films'] if len(planeta['films']) > 0 else '',
                'qtd_filmes': len(planeta['films']),
                'clima': planeta['climate']
            }
            return jsonify({
                'message': 'success',
                'register_updated': dados.modified_count,
                'id_updated': id,
                'nome': obj['nome'],
                'terreno': obj['terreno'],
                'clima': obj['clima'],
            }), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 404

# rota de erro 404
@app.errorhandler(404)
def not_found(e):
    return jsonify(error=str(e)), 404


if __name__ == '__main__':
    app.run()